import express from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";
import { connect_db } from "./db/mongoose.connection.js";
import user_router from "./routers/user.router.js";
import {
    error_handler,
    error_handler2,
    not_found,
    logError
} from "./middleware/errors.handler.js";
import {addIdToReq,logRequest} from "./middleware/reqPreProccesing.js";

class App {
    DB_URI;
    HOST;
    PORT;
    app;
    //ask yariv about this or deafult values .. should i run the function in the
    constructor() {
        const { PORT, HOST, DB_URI } = process.env;

        this.DB_URI = DB_URI;
        this.HOST = HOST || "localhost";
        this.PORT = PORT || 8080;
        this.app = express();

        this.setMiddlewares();
        this.setRoutes();
        this.setErrorHandlers();
        this.conncectToDb();
    }

    setMiddlewares() {
        log.blue("setting Middlewares...");
        this.app.use(cors());
        this.app.use(morgan("dev"));
        this.app.use(addIdToReq);
        this.app.use(logRequest);
    }

    setRoutes() {
        log.blue("setting routes...");

        // this.app.use('/api/stories', story_router);
        this.app.use("/api/users", user_router);
    }

    setErrorHandlers() {
        log.blue("setting error handlers...");

        this.app.use("*", not_found);
        this.app.use(logError);
        this.app.use(error_handler);
        this.app.use(error_handler2);
        //when no routes were matched...
    }

    async conncectToDb() {
        await connect_db(this.DB_URI as string);
    }

    async run() {
        try {
            await this.app.listen(Number(this.PORT), this.HOST);
            log.magenta(
                "api is live on",
                ` ✨ ⚡  http://${this.HOST}:${this.PORT} ✨ ⚡`
            );
        } catch (e) {
            console.log(e);
        }
    }
}

const myApp = new App();

myApp.run();
