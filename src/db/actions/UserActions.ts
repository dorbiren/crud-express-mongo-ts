import user_model from "../models/user.model.js";
import { IUserDetails } from "../../types.js";

export async function addUser(userDetails:IUserDetails) {
    const user = await user_model.create(userDetails);
    return user;
}

export async function getAllUsers() {
    const users = await user_model.find();
    return users;
}

export async function getUserById(userId:string) {
    const user = await user_model.findById(userId);
    return user;
}

export async function updateUser(userId:string,newDetails:Partial<IUserDetails>) {
    const user = await user_model.findByIdAndUpdate(userId,newDetails,{new:true,upsert:false});
    return user;
}

export async function deleteUser(userId:string,) {
    const user = await user_model.findByIdAndRemove(userId);
    return user;
}

export async function getPageOfUsers(page:number,size:number) {
    const users = await user_model
            .find()
            .skip(page * size)
            .limit(size);
    return users;
}

