import mongoose from "mongoose";
const { Schema, model } = mongoose;

const UserSchema = new Schema({
    first_name: {
        type: String,
        required: true,
        minlength: [2, "name is to short"],
    },
    last_name: {
        type: String,
        required: true,
        minlength: [2, "lastname is to short"],
    },
    email: {
        type: String,
        required: true,
        unique: true,
        match: [
            /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/,
            "email address not valid",
        ],
    },
    phone: { type: String, required: true ,match:[/^[0-9]{3}[-s.][0-9]{3}[-s.][0-9]{4}$/,"phone not valid"]},
});

export default model("user", UserSchema);
