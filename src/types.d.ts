export type IUserDetails={
    first_name: string,
    last_name: string,
    email: string,
    phone: string,
}



export declare global {

    namespace Express {
      interface Request {
          users: User[],
          id:string;
      }
    }
  }
