/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../middleware/route.async.wrapper.js";
import express from "express";
import { validateUser } from "../middleware/uservalidator.js";
import { Request, Response } from "express";
import {
    addUser,
    getAllUsers,
    getUserById,
    updateUser,
    deleteUser,
    getPageOfUsers,
} from "../db/actions/UserActions.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW USER;
router.post(
    "/",
    validateUser,
    raw(async (req: Request, res: Response) => {
        const user = await addUser(req.body);
        res.status(200).json(user);
    })
);

// GET ALL USERS
router.get(
    "/",
    raw(async (req: Request, res: Response) => {
        const users = await getAllUsers();
        res.status(200).json(users);
    })
);

// GETS A SINGLE USER
router.get(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const user = await getUserById(req.params.id);
        // .select(`-_id
        //     first_name
        //     last_name
        //     email
        //     phone`);
        if (!user) return res.status(404).json({ status: "No user found." });
        res.status(200).json(user);
    })
);
// UPDATES A SINGLE USER
router.put(
    "/:id",
    validateUser,
    raw(async (req: Request, res: Response) => {
        const user = await updateUser(req.params.id, req.body);
        res.status(200).json(user);
    })
);

// DELETES A USER
router.delete(
    "/:id",
    raw(async (req: Request, res: Response) => {
        const user = await deleteUser(req.params.id);
        if (!user) return res.status(404).json({ status: "No user found." });
        res.status(200).json(user);
    })
);

router.get(
    "/pagination/:page/:size",
    raw(async (req: Request, res: Response) => {
        const page = Number(req.params.page);
        const size = Number(req.params.size);

        if (isNaN(page) || isNaN(size)) {
            throw new Error("page and size should be numbers");
        }
        const users = await getPageOfUsers(page, size);
        res.json(users);
    })
);

export default router;
