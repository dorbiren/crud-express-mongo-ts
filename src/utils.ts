export function generateID() {
    return Math.random().toString(32).slice(2);
}
export function getTimeString() {
    const date = new Date();
    return `${date.toDateString()} ${date.toTimeString()}`;
}
